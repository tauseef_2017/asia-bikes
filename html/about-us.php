<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="css/hover.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>
<header>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="menu">
					<a href=""><img src="img/logo.png"></a>
					<ul>
						<li><a href="">Home</a></li>
						<li><a href="">About us</a></li>
						<li><a href="">Facilities</a></li>
						<li><a href="">Products</a></li>
						<li><a href="">News</a></li>
						<li><a href="">Contact us</a></li>
					</ul>
					<div>
						<a class="btn hvr-shutter-out-vertical" href="">Inquire Now</a>
						<a class="msg" href=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<section class="banner"> 
	<img src="img/about-banner.png">
	<div class="container">
		<div class="cont">
			<h1>About Us</h1>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="cont-cont">
					<p>Asiabike Industrial Limited is a BOI (Board of Investment -Sri Lanka) approved company started in the year 1994, a 100% fully owned local enterprise that manufactures all types of Bikes including high quality mountain, racing, city bikes, children’s, trekking and all terrain bicycles to the European Union and other countries.<br><br>
					We are the leading exporters in Sri Lanka supplying bicycles using the modern state of the art technology for more than 2 decades. Due to the tremendous effort emphasized on production and technical aspects with quality at the forefront, Asiabike has been able to grow leaps and bounds to become a trusted bicycle exporter to leading brands globally.<br>
					Asiabike is one of the main production companies to enjoy preferential trade access to the European Union, enjoying GSP status enabling tariff reductions in EU market entry. This has enabled both Global Buyers and Asiabike to export Bikes in an efficient manner.
 					<br><br>
					We currently cater to the Indian market and some of the leading Bike manufacturers and Brands from India source from Asiabike, making us the largest exporter to the Indian Bike market from Sri Lanka. Asiabike has been able to cater to the high and mass market brands of Indian Buyers to become  a mutually beneficial business partner.

					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="gary">
	<div class="container">
		<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-4">
				<div class="vmv vision" style="background-image: url(img/vision.png);">
					<div>
						<h4>OUR  VISION</h4>
						<p>Generate rewarding value to all our stakeholders. To reward empower and grow our Human Capital. To serve Society with eco friendly transport and recreation.</p>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4">
				<div class="vmv vision" style="background-image: url(img/vision.png);">
					<div>
						<h4>OUR  MISSION</h4>
						<p>Generate rewarding value to all our stakeholders. To reward empower and grow our Human Capital. To serve Society with eco friendly transport and recreation.</p>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4">
				<div class="vmv vision" style="background-image: url(img/values.png);">
					<div>
						<h4>CORPORATE  VALUES</h4>
						<p>Generate rewarding value to all our stakeholders. To reward empower and grow our Human Capital. To serve Society with eco friendly transport and recreation.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="history">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 col-lg-6">
				<div class="his-img">
					<img src="img/highwheeler.png">
				</div>
			</div>
			<div class="col-xl-6 col-lg-6">
				<div class="his">
					<h4>Our <span>History</span></h4>
					<p>Bicycle manufacturing has been a family oriented business for almost 05 decades and before the inception of Asiabike as a fully fledged exporter manufacturing to the local market was the priority.<br><br>
					A fully fledged component and frame building was handled through inception using German based machinery. Therefore management and workers are fully embroiled in bicycle manufacturing, through changes in market demands and customer expectations, Asiabike has evolved with times to create quality products in further establishing as a credible bike manufacturer globally.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="gary directors-main">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="sub-hed">
					<h4>Our <span>Directors</span></h4>
				</div>
			</div>
		</div>
		<div class="owl-carousel owl-theme" id="owl-directors">
	    	<div class="item">
	    		<div class="directors">
					<img src="img/der1.png">
					<h6>A.W. M FAROOK<span>(Chairman)</span></h6>
					<p>Counts almost 50 years experience in the management of Bicycle Industry, His knowledge...</p>
					<a href="">Read more</a>
				</div>	    		
	    	</div>
	    	<div class="item">
	    		<div class="directors">
					<img src="img/der1.png">
					<h6>A.W. M FAROOK<span>(Chairman)</span></h6>
					<p>Counts almost 50 years experience in the management of Bicycle Industry, His knowledge...</p>
					<a href="">Read more</a>
				</div>	    		
	    	</div>
	    	<div class="item">
	    		<div class="directors">
					<img src="img/der1.png">
					<h6>A.W. M FAROOK<span>(Chairman)</span></h6>
					<p>Counts almost 50 years experience in the management of Bicycle Industry, His knowledge...</p>
					<a href="">Read more</a>
				</div>	    		
	    	</div>
		</div>	
	</div>
</section>
<footer>
	<div class="footer-cont">
		<div class="container">
			<div class="row">
				<div class="col-xl-3">
					<div class="footer-logo">
						<a href="">
							<img src="img/logo.png">
						</a>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Hotline</span>
						<h5>+94 38 2232327</h5>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Email Address</span>
						<a href="mailto:asiabike@asiabike.lk">asiabike@asiabike.lk</a>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Like us on</span>
						<ul>
							<li><a href=""><img src="img/face.png"></a></li>
							<li><a href=""><img src="img/ins.png"></a></li>
							<li><a href=""><img src="img/link.png"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xl-4">
				<div class="footer-con">
					<h4>Come Visit Us</h4>
					<p><span>Asia Bike Head Office Sri Lanka</span>
					114, Old Galle Road,<br>
					Henamulla, Panadura, Sri Lanka.
					</p><br>
					<p><span>Asia Bike Plant</span>
					Ratnapura Road, Boralugoda, <br>
					Poruwadanda, Horana, Sri Lanka
					</p>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>About</h4>
					<ul>
						<li><a href="">Vision & Mission </a></li>
						<li><a href="">About company </a></li>
						<li><a href="">Factory</a></li>
						<li><a href="">Achievements  </a></li>
					</ul>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>Products</h4>
					<ul>
						<li><a href="">MTB’s </a></li>
						<li><a href="">Road </a></li>
						<li><a href="">City </a></li>
						<li><a href="">BMX </a></li>
						<li><a href="">Kids Range </a></li>
						<li><a href="">E-bikes </a></li>
					</ul>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>Facilities</h4>
					<ul>
						<li><a href="">Warehouse </a></li>
						<li><a href="">Raw materials handling </a></li>
						<li><a href="">Frame construction </a></li>
						<li><a href="">Decal & painting facilities </a></li>
						<li><a href="">Wheel building </a></li>
						<li><a href="">Assembly line </a></li>
						<li><a href="">Finish goods warehouse </a></li>
						<li><a href="">Quality assurance </a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="copy">
		<div class="container">
			<div class="">
				<p class="left">Copyright 2016 &copy; Asiabike.lk. All Rights Reserved. </p>
				<p class="right">
					<a href="">
						Design 
					</a>
					by
					<a href=""> 
						<img src="img/e.png">EWD
					</a>
				</p>
			</div>
		</div>
	</div>
</footer>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

</body>
</html>