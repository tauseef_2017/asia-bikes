<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="css/hover.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<header>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="menu">
					<a href=""><img src="img/logo.png"></a>
					<ul>
						<li><a href="">Home</a></li>
						<li><a href="">About us</a></li>
						<li><a href="">Facilities</a></li>
						<li><a href="">Products</a></li>
						<li><a href="">News</a></li>
						<li><a href="">Contact us</a></li>
					</ul>
					<div>
						<a class="btn hvr-shutter-out-vertical" href="">Inquire Now</a>
						<a class="msg" href=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<section>
	<div class="silder">
		<div class="owl-carousel owl-theme" id="owl-slider">
	    	<div class="item">
	    		<div class="slider-main">
	    			<img src="img/banner.png">
	    			<div>
	    				<h2>NEW THREADS FOR A NEW SEASON</h2>
	    				<a class="hvr-shutter-out-vertical" href="">Read More</a>
	    			</div>
	    		</div>	    		
	    	</div>
		</div>
	</div>
</section>
<section class="welcome-section">
	<div class="container">
		<div class="row">
			<div class="col-xl-7">
				<div class="welcome">
					<h1>Welcome to <span>Asiabike</span></h1>
					<p>Asiabike is a BOI approved company which started in the year 1994, a 100% fully owned local enterprise that manufactures all types of bikes including high-end MTB’s, Road Bikes, City Bikes, Kids Bikes, BMX’s, E-Bikes and all terrain bicycles to the European Union and other countries.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-5">
				<div class="row">
					<div class="right-bike">
						<div class="quality">
							<h3>Quality at its best</h3>
							<p>From the paint to wheels</p>
							<a class="btn" href="">Explore More</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-7 bike-back">
				<div class="">
					<div class="owl-carousel owl-theme" id="owl-bike">
		    			<div class="item">
		    				<div class="owl-bike">
			    				<img src="img/bike-1.png">
			    				<h6>BMX</h6>
			    				<p>Duis autem vel eum iriure dolor in hendrerit in vulputate</p>
			    				<a class="btn hvr-shutter-out-vertical" href="">Read More</a>
		    				</div>
		    			</div>
		    		</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="progres-heading">
					<h4>Our Global Reach</h4>
				</div>
				<div class="map-img">
					<img  id="map-map" src="img/map/map.png">
					<img class="active" id="map-finland" src="img/map/map-finland.png">
					<img class="" id="map-germany" src="img/map/map-germany.png">
					<img class="" id="map-greece" src="img/map/map-greece.png">
					<img class="" id="map-india" src="img/map/map-india.png">
					<img class="" id="map-uk" src="img/map/map-uk.png">
					<img class="" id="map-netherland" src="img/map/map-netherland.png">
					<span class="map" id="map"></span>
					<span class="map" id="finland"></span>
					<span class="map" id="germany"></span>
					<span class="map" id="greece"></span>
					<span class="map" id="india"></span>
					<span class="map" id="uk"></span>
					<span class="map" id="netherland"></span>
				</div>
			</div>
		</div>
	</div>	
</section>
<section class="relative">
	<div class="ani-bike-section">

		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="progres-heading">
						<h4>Our Product Process</h4>
					</div>
					<div class="proges-bar">
						<div class="dote active dote1">
							<span></span>
							<p>Frame manufacturing</p>
						</div>
						<div class="line"></div>
						<div class="dote dote2">
							<span></span>
							<p>Painting</p>
						</div>
						<div class="line"></div>
						<div class="dote dote3">
							<span></span>
							<p>Sticker process</p>
						</div>
						<div class="line"></div>
						<div class="dote dote4">
							<span></span>
							<p>Wheel assembly</p>
						</div>
						<div class="line"></div>
						<div class="dote dote5">
							<span></span>
							<p>Assembly</p>
						</div>
						<div class="line"></div>
						<div class="dote dote6">
							<span></span>
							<p>Packing</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid mainsf">
			<div class="row">
			<div class="col-xl-6">
				<div class="msg-box">
					<div class="bike-msg" id="bm6">
						<img src="img/icon6.png">
						 <h6>Packing</h6>
						 <p>Assembled bike is then packed inside the carton along with saddle & pedals. Carton opening is stapled & banded & the bike is ready to be shipped.</p>
					</div>
					<div class="bike-msg" id="bm5">
						<img src="img/icon5.png">
						 <h6>Assembly</h6>
						 <p>Painted frames are then fed into assembly department where all parts like wheels, chain wheel, gears, brakes, handle bars etc are fitted, brakes, gears are adjusted. The bikes are then assembled as per 85% or 90% method as per customer’s demand.</p>
					</div>
					<div class="bike-msg" id="bm4">
						<img src="img/icon4.png">
						 <h6>Wheel assembly</h6>
						 <p>Hubs are laced with spokes, laced hubs are then inserted in the rims & tightened on automatic rim tightening machines. The tightened rims are then trued in automatic machines to remove buckle; rims are trued up to a limit of 2mm as per industry norms. On trued rims freewheels, tube & tyres are mounted & complete wheel is ready for assembly.</p>
					</div>
					<div class="bike-msg" id="bm3">
						<img src="img/icon3.png">
						 <h6>Sticker process</h6>
						 <p>There are two types of stickers normal PVC type and water decal. Water decals are pasted through a chemical process on the frames, then dried in oven & after that the lacquer coat is applied. Thus the frames with stickers are ready to be assembled. PVC stickers are pasted on painted frame directly & does not require any further process.</p>
					</div>
					<div class="bike-msg" id="bm2">
						<img src="img/icon2.png">
						 <h6>Painting</h6>
						 <p>Frames are then chemically cleaned known as pre-treatment process. After pre-treatment the frames are painted in automatic disc plant as per prescribed color, baked in oven & are now ready for stickering.</p>
					</div>
					<div class="bike-msg" id="bm1">
						<img src="img/icon1.png">
						 <h6>Frame manufacturing</h6>
						 <p>Mitered tubes are joined as per frame drawing and TIG welded. The ready-made frame is then checked with 1:1 drawing. Seats tube is rimmed & BB shell is threaded. Once the frame (alloy) is ready its put for heat treatment T4 & T6 process. After heat treatment the frame gets hardened to the required strength & is ready for use.</p>
					</div>
				</div>
			</div>
			<div class="col-xl-6">
				<div class="bike-asb">
					<div class="ani-bike">

						<div id="bike5">
							<img class="bike" src="img/bike/bike-5.png">
							<img class="tire-lfet" src="img/bike/tire-lfet.png">
							<img class="tire-right" src="img/bike/tire-right.png">
							<img class="handle" src="img/bike/handle.png">
							<img class="sheet" src="img/bike/sheet.png">
						</div>
						<div id="bike4">
							<img class="bike" src="img/bike/bike-4.png">
							<img class="tire-lfet" src="img/bike/tire-lfet.png">
							<img class="tire-right" src="img/bike/tire-right.png">
						</div>
						<img id="bike3" src="img/bike/bike-3.png">
						<div id="bike2">
							<img class="bike blu act-cor" src="img/bike/bike-2.png">
							<img class="bike red" src="img/bike/bike-2r.png">
							<img class="bike yelw" src="img/bike/bike-2y.png">
							<img class="bike brw" src="img/bike/bike-2b.png">
							<div class="colors-bike">
								<div id="red"><span></span></div>
								<div id="blu" class="act-cor"><span ></span></div>
								<div id="yelw"><span></span></div>
								<div id="brw"><span></span></div>
							</div>
						</div>
						<img id="bike1" class="rem-op" src="img/bike/bike-1.png">
						
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>

</section>
<div class="" style="height: 2130px;"></div>
<div class="rel-up">
	<section>
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="sub-hed">
						<h4>Latest News & <span>Events</span></h4>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-4">
					<div class="blog">
						<img src="img/post.png">
						<span>7th December 2018</span>
						<h6>Küng Leads the Way for BMC Racing Team on a Tough Day of Racing </h6>
						<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim. </p>
						<a class="btn hvr-shutter-out-vertical" href="">Read more</a>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="blog">
						<img src="img/post.png">
						<span>7th December 2018</span>
						<h6>Küng Leads the Way for BMC Racing Team on a Tough Day of Racing </h6>
						<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim. </p>
						<a class="btn hvr-shutter-out-vertical" href="">Read more</a>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="blog">
						<img src="img/post.png">
						<span>7th December 2018</span>
						<h6>Küng Leads the Way for BMC Racing Team on a Tough Day of Racing </h6>
						<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim. </p>
						<a class="btn hvr-shutter-out-vertical" href="">Read more</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="awards">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-6">
					<div class="row">
						<div class="awards-heading">
							<h4>Awards & <span>Certifications</span></h4>
						</div>
						<div class="owl-carousel owl-theme" id="owl-cer">
			    			<div class="item">
			    				<div class="owl-bike">
				    				<img src="img/CER-2.png">			    				
				    				<p>Presidental Export Awards 2018</p>
			    				</div>
			    			</div>
			    		</div>
					</div>
				</div>
				<div class="col-xl-6">
					<div class="row affiliate-color">
						<div class="affiliate">
							<h3><span>What to be an</span> Affiliate?</h3>
							<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
							<a class="btn" href="">Read more</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<div class="footer-cont">
			<div class="container">
				<div class="row">
					<div class="col-xl-3">
						<div class="footer-logo">
							<a href="">
								<img src="img/logo.png">
							</a>
						</div>
					</div>
					<div class="col-xl-3">
						<div class="cont-footer">
							<span>Hotline</span>
							<h5>+94 38 2232327</h5>
						</div>
					</div>
					<div class="col-xl-3">
						<div class="cont-footer">
							<span>Email Address</span>
							<a href="mailto:asiabike@asiabike.lk">asiabike@asiabike.lk</a>
						</div>
					</div>
					<div class="col-xl-3">
						<div class="cont-footer">
							<span>Like us on</span>
							<ul>
								<li><a href=""><img src="img/face.png"></a></li>
								<li><a href=""><img src="img/ins.png"></a></li>
								<li><a href=""><img src="img/link.png"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xl-4">
					<div class="footer-con">
						<h4>Come Visit Us</h4>
						<p><span>Asia Bike Head Office Sri Lanka</span>
						114, Old Galle Road,<br>
						Henamulla, Panadura, Sri Lanka.
						</p><br>
						<p><span>Asia Bike Plant</span>
						Ratnapura Road, Boralugoda, <br>
						Poruwadanda, Horana, Sri Lanka
						</p>
					</div>
				</div>
				<div class="col-xl-2">
					<div class="footer-con">
						<h4>About</h4>
						<ul>
							<li><a href="">Vision & Mission </a></li>
							<li><a href="">About company </a></li>
							<li><a href="">Factory</a></li>
							<li><a href="">Achievements  </a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2">
					<div class="footer-con">
						<h4>Products</h4>
						<ul>
							<li><a href="">MTB’s </a></li>
							<li><a href="">Road </a></li>
							<li><a href="">City </a></li>
							<li><a href="">BMX </a></li>
							<li><a href="">Kids Range </a></li>
							<li><a href="">E-bikes </a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-2">
					<div class="footer-con">
						<h4>Facilities</h4>
						<ul>
							<li><a href="">Warehouse </a></li>
							<li><a href="">Raw materials handling </a></li>
							<li><a href="">Frame construction </a></li>
							<li><a href="">Decal & painting facilities </a></li>
							<li><a href="">Wheel building </a></li>
							<li><a href="">Assembly line </a></li>
							<li><a href="">Finish goods warehouse </a></li>
							<li><a href="">Quality assurance </a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
		<div class="copy">
			<div class="container">
				<div class="">
					<p class="left">Copyright 2016 &copy; Asiabike.lk. All Rights Reserved. </p>
					<p class="right">
						<a href="">
							Design 
						</a>
						by
						<a href=""> 
							<img src="img/e.png">EWD
						</a>
					</p>
				</div>
			</div>
		</div>
	</footer>
</div>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

</body>
</html>