<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="css/hover.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>
<header>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="menu">
					<a href=""><img src="img/logo.png"></a>
					<ul>
						<li><a href="">Home</a></li>
						<li><a href="">About us</a></li>
						<li><a href="">Facilities</a></li>
						<li><a href="">Products</a></li>
						<li><a href="">News</a></li>
						<li><a href="">Contact us</a></li>
					</ul>
					<div>
						<a class="btn hvr-shutter-out-vertical" href="">Inquire Now</a>
						<a class="msg" href=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<section class="banner"> 
	<img src="img/facilities-banner.png">
	<div class="container">
		<div class="cont">
			<h1>Facilities</h1>
		</div>
	</div>
</section>

<section class="service"> 
	<div class="container">
		<div class="row">
			<div class="col-xl-4">
				<div class="sev-nav">
					<ul>
						<li class="active" id="1"> Warehouse</li>
						<li id="2"> Raw materials handling </li>
						<li id="3"> Frame construction </li>
						<li id="4"> Decal & painting facilities </li>
						<li id="5"> Wheel building  </li>
						<li id="6"> Assembly line </li>
						<li id="7"> Finish goods warehouse  </li>
						<li id="8"> Quality assurance  </li>
					</ul>
				</div>
			</div>
			<div class="col-xl-8">
				<div class="sev-cont">
					<div class="active" id="cont-1"> 
						<img src="img/Toogood-Plastics-Factory-IOW.png">
						<h4>Warehouse</h4>
						<p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit.  per inceptos himenaeos. Mauris in erat justo.  per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis da. Etiam pharetra, erat sed fermentum feugiat, Sed ut imperdiet nisi.</p>
						<a class="btn" href="">inquiry</a>
					</div>
					<div id="cont-2"> E City</div>
					<div id="cont-3"> E Folder</div>
					<div id="cont-4"> E Dragon</div>
				</div>
			</div>
		</div>
	</div>
</section>

<footer>
	<div class="footer-cont">
		<div class="container">
			<div class="row">
				<div class="col-xl-3">
					<div class="footer-logo">
						<a href="">
							<img src="img/logo.png">
						</a>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Hotline</span>
						<h5>+94 38 2232327</h5>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Email Address</span>
						<a href="mailto:asiabike@asiabike.lk">asiabike@asiabike.lk</a>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Like us on</span>
						<ul>
							<li><a href=""><img src="img/face.png"></a></li>
							<li><a href=""><img src="img/ins.png"></a></li>
							<li><a href=""><img src="img/link.png"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xl-4">
				<div class="footer-con">
					<h4>Come Visit Us</h4>
					<p><span>Asia Bike Head Office Sri Lanka</span>
					114, Old Galle Road,<br>
					Henamulla, Panadura, Sri Lanka.
					</p><br>
					<p><span>Asia Bike Plant</span>
					Ratnapura Road, Boralugoda, <br>
					Poruwadanda, Horana, Sri Lanka
					</p>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>About</h4>
					<ul>
						<li><a href="">Vision & Mission </a></li>
						<li><a href="">About company </a></li>
						<li><a href="">Factory</a></li>
						<li><a href="">Achievements  </a></li>
					</ul>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>Products</h4>
					<ul>
						<li><a href="">MTB’s </a></li>
						<li><a href="">Road </a></li>
						<li><a href="">City </a></li>
						<li><a href="">BMX </a></li>
						<li><a href="">Kids Range </a></li>
						<li><a href="">E-bikes </a></li>
					</ul>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>Facilities</h4>
					<ul>
						<li><a href="">Warehouse </a></li>
						<li><a href="">Raw materials handling </a></li>
						<li><a href="">Frame construction </a></li>
						<li><a href="">Decal & painting facilities </a></li>
						<li><a href="">Wheel building </a></li>
						<li><a href="">Assembly line </a></li>
						<li><a href="">Finish goods warehouse </a></li>
						<li><a href="">Quality assurance </a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="copy">
		<div class="container">
			<div class="">
				<p class="left">Copyright 2016 &copy; Asiabike.lk. All Rights Reserved. </p>
				<p class="right">
					<a href="">
						Design 
					</a>
					by
					<a href=""> 
						<img src="img/e.png">EWD
					</a>
				</p>
			</div>
		</div>
	</div>
</footer>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

</body>
</html>