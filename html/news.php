<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="css/hover.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style-2.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>
<header>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="menu">
					<a href=""><img src="img/logo.png"></a>
					<ul>
						<li><a href="">Home</a></li>
						<li><a href="">About us</a></li>
						<li><a href="">Facilities</a></li>
						<li><a href="">Products</a></li>
						<li><a href="">News</a></li>
						<li><a href="">Contact us</a></li>
					</ul>
					<div>
						<a class="btn hvr-shutter-out-vertical" href="">Inquire Now</a>
						<a class="msg" href=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<section class="banner"> 
	<img src="img/news-banner.png">
	<div class="container">
		<div class="cont">
			<h1>News</h1>
		</div>
	</div>
</section>
<section class="news-section">
	<div class="container">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-7">
				<div class="news-content">
					<div>
						<span>06</span>
						Oct
						2017
					</div>
					<img src="img/news.png" alt="">
                </div>
                <div class="news-text">
                	<h2>Asiabike launches new collection for 2017-2018 </h2>
                    <p>Asiabike is one of the main production companies to enjoy preferential trade access to the European Union, enjoying GSP status enabling tariff reductions in EU market entry. This has enabled both Global Buyers and Asiabike to export Bikes in an efficient manner</br></br>
                        We currently cater to the Indian market and some of the leading Bike manufacturers and Brands from India source from Asiabike, making us the largest exporter to the Indian Bike market from Sri Lanka. Asiabike has been able to cater to the high and mass market brands of Indian Buyers to become  a mutually beneficial business partner.
					</p>
					
                </div>
                
				<div class="new-share">
                        
                    </ul>	
				</div>
            </div>
            

            <div class="col-xl-4 col-lg-4 col-md-5">
				<div class="Recent-news">
                    <h3 class="news-heading">Recent <span>news</span></h3>
					<hr>
					
                    <div class="list-news">
						<div class="news-img">
							<img src="img/news-1.jpg" alt="">
						</div>
						<div class="news-short">
							<p>Asiabike is one of the main production companies to..</p>
							<span>6 October 2015</span>
						</div>
					</div>
					<div class="list-news">
						<div class="news-img">
							<img src="img/news-1.jpg" alt="">
						</div>
						<div class="news-short">
							<p>Asiabike is one of the main production companies to..</p>
							<span>6 October 2015</span>
						</div>
					</div>
						
				</div>
			</div>
		</div>
	</div>
</section>
<footer>
	<div class="footer-cont">
		<div class="container">
			<div class="row">
				<div class="col-xl-3">
					<div class="footer-logo">
						<a href="">
							<img src="img/logo.png">
						</a>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Hotline</span>
						<h5>+94 38 2232327</h5>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Email Address</span>
						<a href="mailto:asiabike@asiabike.lk">asiabike@asiabike.lk</a>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Like us on</span>
						<ul>
							<li><a href=""><img src="img/face.png"></a></li>
							<li><a href=""><img src="img/ins.png"></a></li>
							<li><a href=""><img src="img/link.png"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xl-4">
				<div class="footer-con">
					<h4>Come Visit Us</h4>
					<p><span>Asia Bike Head Office Sri Lanka</span>
					114, Old Galle Road,<br>
					Henamulla, Panadura, Sri Lanka.
					</p><br>
					<p><span>Asia Bike Plant</span>
					Ratnapura Road, Boralugoda, <br>
					Poruwadanda, Horana, Sri Lanka
					</p>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>About</h4>
					<ul>
						<li><a href="">Vision & Mission </a></li>
						<li><a href="">About company </a></li>
						<li><a href="">Factory</a></li>
						<li><a href="">Achievements  </a></li>
					</ul>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>Products</h4>
					<ul>
						<li><a href="">MTB’s </a></li>
						<li><a href="">Road </a></li>
						<li><a href="">City </a></li>
						<li><a href="">BMX </a></li>
						<li><a href="">Kids Range </a></li>
						<li><a href="">E-bikes </a></li>
					</ul>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>Facilities</h4>
					<ul>
						<li><a href="">Warehouse </a></li>
						<li><a href="">Raw materials handling </a></li>
						<li><a href="">Frame construction </a></li>
						<li><a href="">Decal & painting facilities </a></li>
						<li><a href="">Wheel building </a></li>
						<li><a href="">Assembly line </a></li>
						<li><a href="">Finish goods warehouse </a></li>
						<li><a href="">Quality assurance </a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="copy">
		<div class="container">
			<div class="">
				<p class="left">Copyright 2016 &copy; Asiabike.lk. All Rights Reserved. </p>
				<p class="right">
					<a href="">
						Design 
					</a>
					by
					<a href=""> 
						<img src="img/e.png">EWD
					</a>
				</p>
			</div>
		</div>
	</div>
</footer>


<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>