<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="css/hover.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>
<header>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="menu">
					<a href=""><img src="img/logo.png"></a>
					<ul>
						<li><a href="">Home</a></li>
						<li><a href="">About us</a></li>
						<li><a href="">Facilities</a></li>
						<li><a href="">Products</a></li>
						<li><a href="">News</a></li>
						<li><a href="">Contact us</a></li>
					</ul>
					<div>
						<a class="btn hvr-shutter-out-vertical" href="">Inquire Now</a>
						<a class="msg" href=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<section class="banner"> 
	<img src="img/banner-contact.png">
	<div class="container">
		<div class="cont">
			<h1>Contact US</h1>

		</div>
	</div>
</section>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-7 col-lg-6 col-md-12">
				<div class="cont-info">
					<p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsaum velit.</p>
					<div class="row">
						<div class="col-xl-6 col-lg-6 col-md-6">
							<div class="info-det address">
								<p>Head Office<br>
								114, Old Galle Road,<br>
								Henamulla,Panadura,<br>
								Sri Lanka.</p>

							</div>
						</div>
					
						<div class="col-xl-6 col-lg-6 col-md-6">
							<div class="info-det email">
								<a href="">	asiabike@asiabike.lk </a>
							</div>
							<div class="info-det fax">
								<p>+94 38 2232327</p>
							</div>
							<div class="info-det tel">
								<p>+94 38 2234104</p>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="form">
					<h4>Mail US</h4>
					<div class="row">
						<div class="col-xl-6 col-lg-6 col-md-6">
							<label>Name</label>
							<input type="text" name="">
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6">
							<label>Email</label>
							<input type="text" name="">
						</div>
					</div>
					<div class="row">
						<div class="col-xl-12">
							<label>Subject</label>
							<input type="text" name="">
						</div>
					</div>
					<div class="row">
						<div class="col-xl-12">
							<label>Message</label>
							<textarea></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-xl-12">
							<input type="submit" value="send" name="">
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="col-xl-5 col-lg-6 col-md-12">
				<div class="row">
					<div class="map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126743.63162574457!2d79.7861643989851!3d6.921833528251914!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae253d10f7a7003%3A0x320b2e4d32d3838d!2sColombo!5e0!3m2!1sen!2slk!4v1524559731440" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<footer>
	<div class="footer-cont">
		<div class="container">
			<div class="row">
				<div class="col-xl-3">
					<div class="footer-logo">
						<a href="">
							<img src="img/logo.png">
						</a>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Hotline</span>
						<h5>+94 38 2232327</h5>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Email Address</span>
						<a href="mailto:asiabike@asiabike.lk">asiabike@asiabike.lk</a>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Like us on</span>
						<ul>
							<li><a href=""><img src="img/face.png"></a></li>
							<li><a href=""><img src="img/ins.png"></a></li>
							<li><a href=""><img src="img/link.png"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xl-4">
				<div class="footer-con">
					<h4>Come Visit Us</h4>
					<p><span>Asia Bike Head Office Sri Lanka</span>
					114, Old Galle Road,<br>
					Henamulla, Panadura, Sri Lanka.
					</p><br>
					<p><span>Asia Bike Plant</span>
					Ratnapura Road, Boralugoda, <br>
					Poruwadanda, Horana, Sri Lanka
					</p>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>About</h4>
					<ul>
						<li><a href="">Vision & Mission </a></li>
						<li><a href="">About company </a></li>
						<li><a href="">Factory</a></li>
						<li><a href="">Achievements  </a></li>
					</ul>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>Products</h4>
					<ul>
						<li><a href="">MTB’s </a></li>
						<li><a href="">Road </a></li>
						<li><a href="">City </a></li>
						<li><a href="">BMX </a></li>
						<li><a href="">Kids Range </a></li>
						<li><a href="">E-bikes </a></li>
					</ul>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>Facilities</h4>
					<ul>
						<li><a href="">Warehouse </a></li>
						<li><a href="">Raw materials handling </a></li>
						<li><a href="">Frame construction </a></li>
						<li><a href="">Decal & painting facilities </a></li>
						<li><a href="">Wheel building </a></li>
						<li><a href="">Assembly line </a></li>
						<li><a href="">Finish goods warehouse </a></li>
						<li><a href="">Quality assurance </a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="copy">
		<div class="container">
			<div class="">
				<p class="left">Copyright 2016 &copy; Asiabike.lk. All Rights Reserved. </p>
				<p class="right">
					<a href="">
						Design 
					</a>
					by
					<a href=""> 
						<img src="img/e.png">EWD
					</a>
				</p>
			</div>
		</div>
	</div>
</footer>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

</body>
</html>