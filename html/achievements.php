<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="css/hover.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>
<header>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="menu">
					<a href=""><img src="img/logo.png"></a>
					<ul>
						<li><a href="">Home</a></li>
						<li><a href="">About us</a></li>
						<li><a href="">Facilities</a></li>
						<li><a href="">Products</a></li>
						<li><a href="">News</a></li>
						<li><a href="">Contact us</a></li>
					</ul>
					<div>
						<a class="btn hvr-shutter-out-vertical" href="">Inquire Now</a>
						<a class="msg" href=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<section class="banner"> 
	<img src="img/about-banner.png">
	<div class="container">
		<div class="cont">
			<h1>About Us</h1>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="awod-cont">
					<p>Our service and bicycle production attributes are well recognized locally and internationally, where we have been conferred with numerous awards through local and international Chambers and business institutions.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">

				<div class="owl-carousel owl-theme" id="owl-awd">
	    			<div class="item">
						<div class="awd-div">
							<img src="img/aw-1.png">
							<div>
								<span>
									<h5>Proud winners of the Gold Award in 2007 </h5>
									<p>Extra Large Category (Industrial Sector) awarded by the National Chamber of Exporters of Sri Lanka.</p>
								</span>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="awd-div">
							<img src="img/aw-1.png">
							<div>
								<span>
									<h5>Proud winners of the Gold Award in 2007 </h5>
									<p>Extra Large Category (Industrial Sector) awarded by the National Chamber of Exporters of Sri Lanka.</p>
								</span>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="awd-div">
							<img src="img/aw-1.png">
							<div>
								<span>
									<h5>Proud winners of the Gold Award in 2007 </h5>
									<p>Extra Large Category (Industrial Sector) awarded by the National Chamber of Exporters of Sri Lanka.</p>
								</span>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="awd-div">
							<img src="img/aw-1.png">
							<div>
								<span>
									<h5>Proud winners of the Gold Award in 2007 </h5>
									<p>Extra Large Category (Industrial Sector) awarded by the National Chamber of Exporters of Sri Lanka.</p>
								</span>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="awd-div">
							<img src="img/aw-1.png">
							<div>
								<span>
									<h5>Proud winners of the Gold Award in 2007 </h5>
									<p>Extra Large Category (Industrial Sector) awarded by the National Chamber of Exporters of Sri Lanka.</p>
								</span>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>

<section class="awd-sec">
	<div class="container-fluid">
		<div class="row gry-awd">
			<div class="col-xl-6 col-lg-6">
				<div class="row awrd-gray">
					<div class="aw-div">
						<div class="col-awd">
							<h4>Memberships</h4>
							<p>Asiabike Industrial Limited’s systems are frequently audited by independent bodies through buyer requirement or Company reinforcement increasing our goodwill. Asiabilke operations abide to relevant state and provincial laws and international good manufacturing practices</p>
						</div>
						<div class="col-awd">
							<h4>Quality and certifications</h4>
							<p>Asiabike Industrial Limited’s systems are frequently audited by independent bodies through buyer requirement or Company reinforcement increasing our goodwill. Asiabilke operations abide to relevant state and provincial laws and international good manufacturing practices</p>
						</div>
						<div class="col-awd">
							<h4>New Service and technical audits</h4>
							<p>Asiabike Industrial Limited’s systems are frequently audited by independent bodies through buyer requirement or Company reinforcement increasing our goodwill. Asiabilke operations abide to relevant state and provincial laws and international good manufacturing practices</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6">
				<div class="row affiliate-color awrd">
					<div class="affiliate awrd">
						<h3>Lorem ipsum dolor sit amet</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<footer>
	<div class="footer-cont">
		<div class="container">
			<div class="row">
				<div class="col-xl-3">
					<div class="footer-logo">
						<a href="">
							<img src="img/logo.png">
						</a>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Hotline</span>
						<h5>+94 38 2232327</h5>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Email Address</span>
						<a href="mailto:asiabike@asiabike.lk">asiabike@asiabike.lk</a>
					</div>
				</div>
				<div class="col-xl-3">
					<div class="cont-footer">
						<span>Like us on</span>
						<ul>
							<li><a href=""><img src="img/face.png"></a></li>
							<li><a href=""><img src="img/ins.png"></a></li>
							<li><a href=""><img src="img/link.png"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xl-4">
				<div class="footer-con">
					<h4>Come Visit Us</h4>
					<p><span>Asia Bike Head Office Sri Lanka</span>
					114, Old Galle Road,<br>
					Henamulla, Panadura, Sri Lanka.
					</p><br>
					<p><span>Asia Bike Plant</span>
					Ratnapura Road, Boralugoda, <br>
					Poruwadanda, Horana, Sri Lanka
					</p>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>About</h4>
					<ul>
						<li><a href="">Vision & Mission </a></li>
						<li><a href="">About company </a></li>
						<li><a href="">Factory</a></li>
						<li><a href="">Achievements  </a></li>
					</ul>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>Products</h4>
					<ul>
						<li><a href="">MTB’s </a></li>
						<li><a href="">Road </a></li>
						<li><a href="">City </a></li>
						<li><a href="">BMX </a></li>
						<li><a href="">Kids Range </a></li>
						<li><a href="">E-bikes </a></li>
					</ul>
				</div>
			</div>
			<div class="col-xl-2">
				<div class="footer-con">
					<h4>Facilities</h4>
					<ul>
						<li><a href="">Warehouse </a></li>
						<li><a href="">Raw materials handling </a></li>
						<li><a href="">Frame construction </a></li>
						<li><a href="">Decal & painting facilities </a></li>
						<li><a href="">Wheel building </a></li>
						<li><a href="">Assembly line </a></li>
						<li><a href="">Finish goods warehouse </a></li>
						<li><a href="">Quality assurance </a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="copy">
		<div class="container">
			<div class="">
				<p class="left">Copyright 2016 &copy; Asiabike.lk. All Rights Reserved. </p>
				<p class="right">
					<a href="">
						Design 
					</a>
					by
					<a href=""> 
						<img src="img/e.png">EWD
					</a>
				</p>
			</div>
		</div>
	</div>
</footer>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

</body>
</html>