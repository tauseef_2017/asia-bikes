$(document).ready(function(){

    // slider
    $('#owl-slider').owlCarousel({
        loop:true,
        dots:false,
        nav:false,
        items:1
    });
    $('#owl-awd').owlCarousel({
        loop:true,
        dots:true,
        nav:false,
        margin:0,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:3,
            },
            1000:{
                items:4
            }
        }
        
    });
    $('#owl-bike').owlCarousel({
        loop:false,
        dots:false,
        nav:true,
        items:1
    });

    $('#owl-cer').owlCarousel({
        loop:true,
        nav:true,
        items:1
    });

    $('#owl-directors').owlCarousel({
        loop:false,
        nav:false,
        margin:50,
        items:3,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:2,
                loop:true,
                nav:false,
                dots:true,
            },
            1000:{
                items:3,
            }
        }
    });


    $(".map").click(function(){
        $('.map-img img').removeClass("active");
        popup = $(this).attr('id');
        $("#map-"+popup).addClass("active");
    });

    $(".mobile-menu").click(function(){
        $('#menu-primary-menu').toggleClass('open-menu')
    });

    $(".col-awd h4").click(function(){
        $('.col-awd h4').removeClass("open-awd");
        $(this).toggleClass('open-awd');
    });

    $(".baike-nav ul li").click(function(){
        $('.baike-nav ul li').removeClass("active");
        $(this).addClass("active");
        activ = $(this).attr('id');

        $('.baike-cont div').removeClass("active");
        $("#cont-"+activ).addClass("active");
    });

    $(".sev-nav ul li").click(function(){
        $('.sev-nav ul li').removeClass("active");
        $(this).addClass("active");
        activ = $(this).attr('id');

        $('.sev-cont div').removeClass("active");
        $("#cont-"+activ).addClass("active");
    });

    var width = $(window).width();

    if(width <= 680){
        $(".mobile-footer h4").click(function(){
            $(this).toggleClass('open-fot');
         });

        $(".baike-nav ul li").click(function(){
            $('.baike-nav ul li').removeClass("active");
            $(this).addClass('active');
            $('.res-drop').removeClass("active");
         });
        $(".res-drop").click(function(){
            $(this).toggleClass('active');
         });


    };
    


// bike animetions in home


    var height = $(window).height();
    $(".relative").css('height',height);

    function animet_slid(a, b, c, d,img_id, hid) {

        if (c >= b) {
            $(a).addClass("bike-msg-rm");
            $(d).addClass("active");
            $(img_id).addClass("rem-op");
            $(hid).addClass("hid");
        }else if(c < b){
            $(a).removeClass("bike-msg-rm");
            $(d).removeClass("active");            
            $(img_id).removeClass("rem-op");            
            $(hid).removeClass("hid");            
        };

    };

    function active_color(id, clas) {
        $(id).click(function(){
            $("#bike2 img").removeClass("act-cor");
            $(".colors-bike div").removeClass("act-cor");
            $(clas).addClass("act-cor");
            $(id).addClass("act-cor");
        });
    };


    $(window).scroll(function() { 

        var scroll = $(window).scrollTop();

        if (scroll >= 500) {
            $(".menu").addClass("stickmenu");
            $("header").addClass("menu-gray");
        } else {
            $(".menu").removeClass("stickmenu");
            $("header").removeClass("menu-gray");
        }



        if(width >= 1031){

            var top = $('.ani-bike-section').offset().top;
            
            if (2259 <= scroll) {
                $(".ani-bike-section").addClass("fixed-ani");
            }
            if (2257 >= scroll) {
                $(".ani-bike-section").removeClass("fixed-ani");
            }
                    
            animet_slid('#bm1', '2460', top, '.dote2','#bike2','#bike1');
            animet_slid('#bm2', '2860', top, '.dote3','#bike3','#bike2');
            animet_slid('#bm3', '3260', top, '.dote4','#bike4','#bike3');
            animet_slid('#bm4', '3760', top, '.dote5','#bike5','#bike4');
            animet_slid('#bm5', '4160', top, '.dote6','#bike6','');
            animet_slid('#bm6', '4360', top, '.dote7','#bike7','');
            
            if (4360 <= scroll) {
                $("#bike5").addClass("go-riht");
            }else{
                $("#bike5").removeClass("go-riht");
            }

        }else if(width <= 1030){

            var top = $('.ani-bike-section').offset().top;
            
            if (2040 <= scroll) {
                $(".ani-bike-section").addClass("fixed-ani");
            }
            if (2038 >= scroll) {
                $(".ani-bike-section").removeClass("fixed-ani");
            }
                    
            animet_slid('#bm1', '2460', top, '.dote2','#bike2','#bike1');
            animet_slid('#bm2', '2860', top, '.dote3','#bike3','#bike2');
            animet_slid('#bm3', '3260', top, '.dote4','#bike4','#bike3');
            animet_slid('#bm4', '3760', top, '.dote5','#bike5','#bike4');
            animet_slid('#bm5', '4160', top, '.dote6','#bike6','');
            animet_slid('#bm6', '4360', top, '.dote7','#bike7','');
            
            if (4360 <= scroll) {
                $("#bike5").addClass("go-riht");
            }else{
                $("#bike5").removeClass("go-riht");
            }

        }else if(width <= 800){
            var top = $('.ani-bike-section').offset().top;
            
            if (1709 <= scroll) {
                $(".ani-bike-section").addClass("fixed-ani");
            }
            if (1705 >= scroll) {
                $(".ani-bike-section").removeClass("fixed-ani");
            }
                    
            animet_slid('#bm1', '2460', top, '.dote2','#bike2','#bike1');
            animet_slid('#bm2', '2860', top, '.dote3','#bike3','#bike2');
            animet_slid('#bm3', '3260', top, '.dote4','#bike4','#bike3');
            animet_slid('#bm4', '3760', top, '.dote5','#bike5','#bike4');
            animet_slid('#bm5', '4160', top, '.dote6','#bike6','');
            animet_slid('#bm6', '4360', top, '.dote7','#bike7','');
            
            if (4360 <= scroll) {
                $("#bike5").addClass("go-riht");
            }else{
                $("#bike5").removeClass("go-riht");
            }

        };

    });

    active_color('#red', '.red');
    active_color('#blu', '.blu');
    active_color('#yelw', '.yelw');
    active_color('#brw', '.brw');

//end bike animetions in home  
});