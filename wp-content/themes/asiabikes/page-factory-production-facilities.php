<?php
get_header();
while(have_posts()): the_post()
?>

<section class="banner">
    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
    <div class="container">
        <div class="cont">
            <h1><?php the_title(); ?></h1>
            <?php the_breadcrumb(); ?>
        </div>
    </div>
</section>

<section class="row-gry">
    <div class="container-fluid">
        <?php
            $left_block = get_field('left_content');
                if($left_block):
        ?>
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12 order-xs-2">
                <div class="row flex-end">
                    <div class="cat-hed arow-right">
                        <h4><?php echo $left_block['title']; ?></h4>
                        <p><?php echo $left_block['description']; ?></p>
                        <div><span>Address -</span>
                            <?php echo $left_block['address']; ?>
                        </div>
                        <div>
                            <span>Tel - </span>
                            <?php echo $left_block['tel']; ?>
                        </div>
                        <div>
                            <span>Email -</span>
                            <?php echo $left_block['email']; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 order-xs-1">
                <div class="row">
                    <div class="fac-img ">
                        <img src="<?php echo $left_block['image']; ?>">
                    </div>
                </div>
            </div>
        </div>
        <?php
            endif;

            $left_block = get_field('right_content');
            if($left_block):
        ?>
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12">
                <div class="row">
                    <div class="fac-img">
                        <img src="<?php echo $left_block['image']; ?>">
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12">
                <div class="row ">
                    <div class="cat-hed pad-left arow-left">
                        <h4><?php echo $left_block['title']; ?></h4>
                        <p><?php echo $left_block['description']; ?></p>
                        <div><span>Address -</span>
                            <?php echo $left_block['address']; ?>
                        </div>
                        <div>
                            <span>Tel - </span>
                            <?php echo $left_block['tel']; ?>
                        </div>
                        <div>
                            <span>Email -</span>
                            <?php echo $left_block['email']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <?php endif; ?>
    </div>
</section>

<?php
    endwhile;
get_footer();
?>