<?php
get_header();
if (is_tax() || is_category() || is_tag() ):
    $qobj = get_queried_object();
    // var_dump($qobj); // debugging only

    // concatenate the query
    $args = array(
        'posts_per_page' => 4,
        'post_type'     => 'ab_products',
        'tax_query' => array(
            array(
                'taxonomy' => $qobj->taxonomy,
                'field' => 'id',
                'terms' => $qobj->term_id,
            )
        ),
        'orderby' => 'post_id',
        'order'   => 'ASC'
    );
    ?>
    <section class="banner">
        <img src="<?php echo get_field('cover_image', $qobj); ?>">
        <div class="container">
            <div class="cont">
                <h1><?php echo $qobj->name; ?></h1>
                <div class="breadcrumbs"><a href="<?php echo home_url();?>">Asia bikes</a> &gt; <a href="<?php echo site_url('products');?>">Products</a> &gt; <?php echo $qobj->name; ?></div>
            </div>
        </div>
    </section>
    <section class="bik-con">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="baike-nav">

                        <span class="res-drop"></span>
                        <ul>
                            <?php
                            $i = 0;
                            $products = new WP_Query( $args );
                                if ($products->have_posts()) :
                                    while ($products->have_posts()) : $products->the_post(); $i++;
                            ?>
                            <li class="<?php echo ($i == 1)? 'active' : ''; ?>" id="<?php echo sanitize_title(get_the_title()); ?>"> <?php the_title(); ?></li>
                                <?php
                                        endwhile;
                                    endif;
                                ?>
                        </ul>
                    </div>
                    <div class="baike-cont">
                        <?php
                            $i = 0;
                            if ($products->have_posts()) :
                                while ($products->have_posts()) : $products->the_post(); $i++;
                        ?>
                        <div class="<?php echo ($i == 1)? 'active' : ''; ?>" id="cont-<?php echo sanitize_title(get_the_title()); ?>">
                            <div class="bike-cont">
                                <img src="<?php the_field('product_image'); ?>">
                                <?php the_content(); ?>
                                <a class="btn" href="<?php echo site_url('contact-us/?your-subject='.get_the_title()); ?>">Enquire Now</a>
                            </div>
                            <div class="specifications">
                                <h4>SPECIFICATIONS</h4>
                                <ul>
                                    <?php
                                        if(have_rows('specification')):
                                            while(have_rows('specification')): the_row();

                                    ?>
                                    <li><span><?php the_sub_field('title'); ?></span>
                                        <?php the_sub_field('description'); ?>
                                    </li>
                                    <?php endwhile; endif; ?>
                                </ul>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <?php
                                endwhile;
                            endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
    endif;
get_footer();
?>