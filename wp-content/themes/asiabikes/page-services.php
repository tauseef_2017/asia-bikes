<?php
get_header();
while(have_posts()): the_post();
$post_id = get_the_ID();
?>
<section class="banner">
    <img src="<?php the_post_thumbnail_url(); ?>">
    <div class="container">
        <div class="cont">
            <h1><?php the_title(); ?></h1>
            <?php the_breadcrumb(); ?>
        </div>
    </div>
</section>

<section class="service">
    <div class="container">
        <div class="row">
    <?php
    $args = array(
        'posts_per_page' => -1,
        'post_type'     => 'services',
        'order_by'      => 'date',
        'order'         => 'ASC'
    );
    // Custom query.
    $query = new WP_Query( $args );
    if ( $query->have_posts() ) :
            ?>
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="sev-nav">
                    <span class="res-drop-ipad"></span>
                    <ul>
                        <?php
                        $i = 0;
                            while ( $query->have_posts() ) : $query->the_post();
                                $i++;
                        ?>
                        <li class="<?php echo ($i == 1)? 'active': ''; ?>" id="<?php echo $i; ?>"> <?php the_title(); ?></li>
                       <?php endwhile; wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-12">
                <div class="sev-cont">
                <?php
                $i = 0;
                     while ( $query->have_posts() ) : $query->the_post();
                            $i++;
                    ?>
                    <div class="<?php echo ($i == 1)? 'active': ''; ?> service-tab" id="cont-<?php echo $i; ?>">
                        <img src="<?php the_post_thumbnail_url(); ?>">
                        <h4><?php the_title(); ?></h4>
                        <?php the_content(); ?>
                        <a class="btn" href="<?php echo site_url('contact-us?subject='.sanitize_title(get_the_title())); ?>">inquiry</a>
                    </div>
                     <?php endwhile; wp_reset_query(); ?>
                </div>
            </div>
        <?php endif; ?>
        </div>
    </div>
</section>
    <?php
endwhile;
get_footer();
?>