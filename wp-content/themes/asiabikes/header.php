<!DOCTYPE html>
<html>
<head>
    <title><?php the_title(); ?> | Asia Bikes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>
<body>
<header class="<?php echo (!is_front_page())? 'inner_page' : ''; ?>">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="menu">
                    <div class="mobile-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <a href="<?php echo home_url(); ?>"><?php
                                        $custom_logo_id = get_theme_mod( 'custom_logo' );
                                        $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );

                                        if ( has_custom_logo() ) {
                                            echo '<img src="'. esc_url( $logo[0] ) .'" alt="'.get_bloginfo( 'name' ).'">';
                                        } else {
                                            echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
                                        }
                                        ?>
                    </a>
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'header-menu',
                        'container' => false,
                        'container_class' => '',
                        'menu_id' => '',
                        'menu_class' => '') );
                    ?>
                    <div>
                        <a class="btn hvr-shutter-out-vertical" href="<?php echo site_url('contact-us'); ?>">Inquire Now</a>
                        <a class="msg" href="mailto:asiabike@asiabike.lk"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>