<footer style="position: relative; z-index: 200">
    <div class="footer-cont">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="footer-logo">
                        <a href="">
                            <img src="<?php echo ASSETS_IMAGE; ?>logo.png">
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="cont-footer">
                        <span>Hotline</span>
                        <a  href="tel:+94 38 2232327">+94 38 2232327</a>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="cont-footer">
                        <span>Email Address</span>
                        <a href="mailto:asiabike@asiabike.lk">asiabike@asiabike.lk</a>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="cont-footer">
                        <span>Like us on</span>
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer-social',
                            'container' => false,
                            'container_class' => '',
                            'menu_id' => '',
                            'menu_class' => '') );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-3">
                <div class="footer-con mobile-footer">
                    <h4>Come Visit Us</h4>
                    <div>
                        <?php if ( is_active_sidebar( 'ewd_footer_bottom' ) ) :  ?>
                        <?php dynamic_sidebar( 'ewd_footer_bottom' ); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3">
                <div class="footer-con mobile-footer">
                    <h4>About</h4>
                    <div>
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer-about',
                            'container' => false,
                            'container_class' => '',
                            'menu_id' => '',
                            'menu_class' => '') );
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3">
                <div class="footer-con mobile-footer">
                    <h4>Products</h4>
                    <div>
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer-products',
                            'container' => false,
                            'container_class' => '',
                            'menu_id' => '',
                            'menu_class' => '') );
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-3 col-md-3">
                <div class="footer-con mobile-footer">
                    <h4>Facilities</h4>
                    <div>
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer-facilities',
                            'container' => false,
                            'container_class' => '',
                            'menu_id' => '',
                            'menu_class' => '') );
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="copy">
        <div class="container">
            <div class="">
                <p class="left">Copyright <?php echo date('Y'); ?> &copy; Asiabike.lk. All Rights Reserved. </p>
                <p class="right">
                    <a href="https://www.extremewebdesigners.com/services/web-design-sri-lanka/">
                        Design
                    </a>
                    by
                    <a href="https://www.extremewebdesigners.com/">
                        <img src="<?php echo ASSETS_IMAGE; ?>e.png"> EWD
                    </a>
                </p>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>