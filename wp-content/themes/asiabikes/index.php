<?php
get_header();
?>

    <section class="banner">
        <img src="<?php echo get_the_post_thumbnail_url(13); ?>">
        <div class="container">
            <div class="cont">
                <h1>News</h1>
                <?php the_breadcrumb(); ?>
            </div>
        </div>
    </section>

    <section class="news-lis">
        <div class="container">
            <div class="row">
                <?php while(have_posts()): the_post(); ?>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="news">
                        <div>
                            <img src="<?php echo get_field('grid_image'); ?>">
                            <div>
                                <span> <?php the_date('d'); ?></span>
                                <?php echo get_the_date('M y'); ?>
                            </div>
                        </div>
                        <h4><?php the_title(); ?></h4>
                        <p><?php echo mb_strimwidth(strip_tags(get_the_content()), 0, 120, '...')?> </p>
                        <a href="<?php the_permalink(); ?>">Read More</a>
                    </div>
                </div>
               <?php endwhile; ?>
            </div>
        </div>
    </section>

<?php get_footer(); ?>