<?php

function register_ewd_menus() {
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'footer-about' => __( 'Footer About' ),
            'footer-products' => __( 'Footer Products' ),
            'footer-facilities' => __( 'Footer Facilities' ),
            'footer-social' => __( 'Footer Social' ),
        )
    );
}