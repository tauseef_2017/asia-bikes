<?php

function ewd_register_resources(){


    wp_register_style('ewd_bootstrap',get_template_directory_uri().'/assets/css/bootstrap-reboot.min.css');
    wp_register_style('ewd_bootstrap_grid',get_template_directory_uri().'/assets/css/bootstrap-grid.min.css');
    wp_register_style('ewd_owd_carousal',get_template_directory_uri().'/assets/css/owl.carousel.min.css');
    wp_register_style('ewd_owl_theme',get_template_directory_uri().'/assets/css/owl.theme.default.min.css');
    wp_register_style('ewd_hover',get_template_directory_uri().'/assets/css/hover.css');
    wp_register_style('ewd_style',get_template_directory_uri().'/assets/css/style.css');
    wp_register_style('ewd_responsive',get_template_directory_uri().'/assets/css/responsive.css');


    wp_register_script('ewd_jquery',get_template_directory_uri().'/assets/js/jquery-3.3.1.min.js',array(),null,true);
    wp_register_script('ewd_owd_carousal',get_template_directory_uri().'/assets/js/owl.carousel.js',array(),null,true);
    wp_register_script('ewd_custom',get_template_directory_uri().'/assets/js/custom.js',array(),null,true);

    wp_enqueue_style('ewd_bootstrap');
    wp_enqueue_style('ewd_bootstrap_grid');
    wp_enqueue_style('ewd_owd_carousal');
    wp_enqueue_style('ewd_owl_theme');
    wp_enqueue_style('ewd_hover');
    wp_enqueue_style('ewd_style');
    wp_enqueue_style('ewd_responsive');

    wp_enqueue_script('ewd_jquery');
    wp_enqueue_script('ewd_owd_carousal');
    wp_enqueue_script('ewd_custom');


}