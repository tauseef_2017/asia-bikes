<?php
    get_header();
    while(have_posts()): the_post()
?>

<section class="banner">
    <img src="<?php echo get_the_post_thumbnail_url(); ?>">
    <div class="container">
        <div class="cont">
            <h1><?php the_title(); ?></h1>
            <?php the_breadcrumb(); ?>
        </div>
    </div>
</section>
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-7 col-lg-6 col-md-12">
                <div class="cont-info">
                    <?php the_content(); ?>
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="info-det address">
                                <p><?php the_field('address'); ?></p>
                            </div>
                            <br>
                            <div class="info-det address">
                                <p><?php the_field('address_copy'); ?></p>

                            </div>
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <div class="info-det email">
                                <a href="mailto:<?php the_field('email'); ?>">	<?php the_field('email'); ?></a>
                            </div>
                            <div class="info-det fax">
                                <p><?php the_field('fax'); ?></p>
                            </div>
                            <div class="info-det tel">
                                <p><a href="tel:<?php the_field('phone'); ?>"><?php the_field('phone'); ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="form">
                    <h4>Mail US</h4>
                    <?php echo do_shortcode('[contact-form-7 id="115" title="Contact us"]'); ?>
                </div>
                <div class="clear"></div>
            </div>
            <div class="col-xl-5 col-lg-6 col-md-12">
                <div class="row">
                    <div class="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.1942278327447!2d79.89922261431666!3d6.74614969512328!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae245e948f3f1f9%3A0xc75ba493384b3ed2!2sAsia+Bike!5e0!3m2!1sen!2slk!4v1525413267868" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
        endwhile;
    get_footer();
?>