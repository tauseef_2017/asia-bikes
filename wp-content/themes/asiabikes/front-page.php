<?php
    get_header();
    while(have_posts()): the_post();
        $slider = get_field('slider');
        if($slider):
            $post = $slider;
            setup_postdata( $post );
?>
<section>
    <div class="silder">
        <div class="owl-carousel owl-theme" id="owl-slider">
            <?php
                $slides = get_field('slides');
                    foreach($slides as $slide):
            ?>
            <div class="item">
                <div class="slider-main">
                    <img src="<?php echo $slide['image']['url']; ?>">
                    <div>
                        <h2><?php echo $slide['description']; ?></h2>
                        <a class="hvr-shutter-out-vertical" href="<?php echo $slide['link']['url']; ?>"><?php echo $slide['link']['title']; ?></a>
                    </div>
                </div>
            </div>
            <?php
                endforeach;
            ?>
        </div>
    </div>
</section>
<?php
        endif;
        wp_reset_postdata();
?>
<section class="welcome-section" style="background-image: url(<?php the_field('bg_image')?>)">
    <div class="container">
        <div class="row">
            <div class="col-xl-7">
                <div class="welcome">
                    <h1><?php the_field('welcome_title'); ?></h1>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section >
    <div class="container-fluid quality-div">
        <div class="row">
            <div class="col-xl-5 col-md-5">
                <div class="row">
                    <div class="right-bike" style="background-image: url(<?php the_field('product_image')?>)">
                        <div class="quality">
                            <h3><?php the_field('product_title'); ?></h3>
                            <p><?php the_field('product_sub_title'); ?></p>
                            <a class="btn" href="<?php the_field('link'); ?>">Explore More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-7 col-md-7 bike-back">
                <div class="">
                    <?php
                        $terms = get_field('categories');
                        krsort($terms);
                        if( $terms ):
                    ?>
                    <div class="owl-carousel owl-theme" id="owl-bike">
                            <?php foreach($terms as $term): ?>
                        <div class="item">
                            <div class="owl-bike">
                                <img src="<?php the_field('image', $term); ?>">
                                <h6><?php echo $term->name; ?></h6>
                                <p><?php echo $term->description; ?></p>
                                <a class="btn hvr-shutter-out-vertical" href="<?php echo get_term_link( $term ); ?>">View More</a>
                            </div>
                        </div>
                            <?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clear"></div>
<section>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="progres-heading">
                    <h4>Our Global Reach</h4>
                </div>
                <div class="mobile-map">
                    <div>
                         <img src="<?php echo ASSETS_IMAGE; ?>map/mobile/itlay.png">
                         <span>itlay</span>
                    </div>
                    <div>
                        <img src="<?php echo ASSETS_IMAGE; ?>map/mobile/findland.png">
                        <span>findland</span>
                    </div>
                    <div>
                        <img src="<?php echo ASSETS_IMAGE; ?>map/mobile/germany.png">
                        <span>germany</span>
                    </div>
                    <div>
                        <img src="<?php echo ASSETS_IMAGE; ?>map/mobile/greece.png">
                        <span>greece</span>
                    </div>
                    <div>
                        <img src="<?php echo ASSETS_IMAGE; ?>map/mobile/india.png">
                        <span>india</span>
                    </div>
                    <div>
                        <img src="<?php echo ASSETS_IMAGE; ?>map/mobile/uk.png">
                        <span>united kingdom</span>
                    </div>
                    <div>
                        <img src="<?php echo ASSETS_IMAGE; ?>map/mobile/ireland.png">
                        <span>ireland</span>
                    </div>
                    <div>
                        <img src="<?php echo ASSETS_IMAGE; ?>map/mobile/netherland.png">
                        <span>netherland</span>
                    </div>
                    <div>
                        <img src="<?php echo ASSETS_IMAGE; ?>map/mobile/poland.png">
                        <span>poland</span>
                    </div>
                </div>
                <div class="map-img">
                    <img  id="map-map" src="<?php echo ASSETS_IMAGE; ?>map/map.png">
                    <img class="active" id="map-finland" src="<?php echo ASSETS_IMAGE; ?>map/map-finland.png">
                    <img class="" id="map-germany" src="<?php echo ASSETS_IMAGE; ?>map/map-germany.png">
                    <img class="" id="map-greece" src="<?php echo ASSETS_IMAGE; ?>map/map-greece.png">
                    <img class="" id="map-india" src="<?php echo ASSETS_IMAGE; ?>map/map-india.png">
                    <img class="" id="map-uk" src="<?php echo ASSETS_IMAGE; ?>map/map-uk.png">
                    <img class="" id="map-netherland" src="<?php echo ASSETS_IMAGE; ?>map/map-netherland.png">
                    <img class="" id="map-ireland" src="<?php echo ASSETS_IMAGE; ?>map/map-ireland.png">
                    <img class="" id="map-poland" src="<?php echo ASSETS_IMAGE; ?>map/map-poland.png">
                    <img class="" id="map-italy" src="<?php echo ASSETS_IMAGE; ?>map/map-italy.png">
                    <span class="map" id="map"></span>
                    <span class="map" id="finland"></span>
                    <span class="map" id="germany"></span>
                    <span class="map" id="greece"></span>
                    <span class="map" id="india"></span>
                    <span class="map" id="uk"></span>
                    <span class="map" id="netherland"></span>
                    <span class="map" id="ireland"></span>
                    <span class="map" id="poland"></span>
                    <span class="map" id="italy"></span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <span id="mousewheel">0</span>
<span id="mousewheel-counter">0</span> -->
<section class="relative hd-mobile">
    <div class="ani-bike-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="progres-heading">
                        <h4><?php the_field('process_title'); ?></h4>
                    </div>
                    <?php
                        if(have_rows('steps')):
                            $step = 0;
                    ?>
                    <div class="proges-bar">
                        <?php
                            while(have_rows('steps')): the_row();
                                $step++;
                        ?>
                        <div class="dote <?php echo 'dote'.$step; echo ($step == 1)? ' active' : ''; ?> ">
                            <span></span>
                            <p><?php the_sub_field('title'); ?></p>
                        </div>
                                <?php if($step < 6): ?>
                        <div class="line"></div>
                         <?php
                                endif;
                            endwhile;
                        ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="container-fluid mainsf">
            <div class="row">
                <div class="col-xl-6 col-lg-5 col-md-12">
                    <div class="msg-box">
                        <?php
                            $items = array_reverse(get_field('steps'));
                            if($items):
                                foreach($items as $item):
                        ?>
                        <div class="bike-msg" id="bm<?php echo $step; $step--; ?>">
                            <img src="<?php echo $item['image']; ?>">
                            <h6><?php echo $item['title']; ?></h6>
                            <p><?php echo $item['description']; ?></p>
                        </div>
                        <?php
                                endforeach;
                            endif;
                        ?>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-7 col-md-12">
                    <div class="bike-asb">
                        <div class="ani-bike">
                            <div id="bike5">
                                <img class="bike" src="<?php echo ASSETS_IMAGE; ?>bike/bike-5.png">
                                <img class="tire-lfet" src="<?php echo ASSETS_IMAGE; ?>bike/tire-lfet.png">
                                <img class="tire-right" src="<?php echo ASSETS_IMAGE; ?>bike/tire-right.png">
                                <img class="handle" src="<?php echo ASSETS_IMAGE; ?>bike/handle.png">
                                <img class="sheet" src="<?php echo ASSETS_IMAGE; ?>bike/sheet.png">
                            </div>
                            <div id="bike4">
                                <img class="bike" src="<?php echo ASSETS_IMAGE; ?>bike/bike-4.png">
                                <img class="tire-lfet" src="<?php echo ASSETS_IMAGE; ?>bike/tire-lfet.png">
                                <img class="tire-right" src="<?php echo ASSETS_IMAGE; ?>bike/tire-right.png">
                            </div>
                            <img id="bike3" src="<?php echo ASSETS_IMAGE; ?>bike/bike-3.png">
                            <div id="bike2">
                                <img class="bike blu act-cor" src="<?php echo ASSETS_IMAGE; ?>bike/bike-2.png">
                                <img class="bike red" src="<?php echo ASSETS_IMAGE; ?>bike/bike-2r.png">
                                <img class="bike yelw" src="<?php echo ASSETS_IMAGE; ?>bike/bike-2y.png">
                                <img class="bike brw" src="<?php echo ASSETS_IMAGE; ?>bike/bike-2b.png">
                                <div class="colors-bike">
                                    <div id="red"><span></span></div>
                                    <div id="blu" class="act-cor"><span ></span></div>
                                    <div id="yelw"><span></span></div>
                                    <div id="brw"><span></span></div>
                                </div>
                            </div>
                            <img id="bike1" class="rem-op" src="<?php echo ASSETS_IMAGE; ?>bike/bike-1.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="hd-mobile" style="height: 2130px;"></div>
<div class="rel-up">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                <?php 
                $news = get_field('news');
                if($news): ?>
                    <div class="sub-hed">
                        <h4><?php the_field('news_title'); ?></h4>
                    </div>
                <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <?php
                    
                    if($news):
                        foreach($news as $post):
                            setup_postdata($post);
                ?>
                <div class="col-xl-4 col-md-4">
                    <div class="blog">
                        <img src="<?php echo get_field('grid_image'); ?>">
                        <span><?php echo get_the_date(); ?></span>
                        <h6><?php the_title(); ?> </h6>
                        <p><?php echo mb_strimwidth(strip_tags(get_the_content()), 0, 120, '...')?> </p>
                        <a class="btn hvr-shutter-out-vertical" href="<?php the_permalink(); ?>">Read more</a>
                    </div>
                </div>
                <?php
                            endforeach;
                        wp_reset_postdata();
                        endif;
                ?>
            </div>
        </div>
    </section>
    <section class="awards">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="row">
                        <div class="awards-heading">
                            <h4>Awards & <span>Certifications</span></h4>
                        </div>
                        <div class="owl-carousel owl-theme" id="owl-cer">
        <?php
        $posts = get_field('awards');
        if( $posts ):
            foreach( $posts as $post):
                setup_postdata($post);
                ?>
                            <div class="item">
                                <div class="owl-bike">
                                    <img src="<?php the_post_thumbnail_url() ?>">
                                    <p><?php the_title() ?></p>
                                </div>
                            </div>
                <?php
            endforeach;
        endif;
        wp_reset_postdata();
        ?>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <?php
                        $get = get_field('right_block');
                        if($get):
                    ?>
                    <div class="row affiliate-color" style="background-image: url(<?php echo $get['image']; ?>); background-repeat: no-repeat; background-size: cover">
                        <div class="affiliate">
                            <h3><?php echo $get['title']; ?></h3>
                            <p><?php echo $get['description']; ?></p>
                            <a class="btn" href="<?php echo site_url('achievements'); ?>">Read more</a>
                        </div>
                    </div>
                    <?php
                        endif;
                    ?>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
        endwhile;
    get_footer();
?>