<?php
    get_header();
?>

<section class="banner">
    <img src="img/news-list.jpg">
    <div class="container">
        <div class="cont">
            <h1>News</h1>
        </div>
    </div>
</section>

<section class="news-lis">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="news">
                    <div>
                        <img src="img/china-show.png">
                        <div>
                            <span>06</span>
                            Oct
                            2017
                        </div>
                    </div>
                    <h4>Asiabike launches new collection for 2017-18</h4>
                    <p>Asiabike marketing Team under the guidance and Leadership of our Managing Director Mr. Isthiark Farook participates...</p>
                    <a href="">Read More</a>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="news">
                    <div>
                        <img src="img/china-show.png">
                        <div>
                            <span>06</span>
                            Oct
                            2017
                        </div>
                    </div>
                    <h4>Asiabike launches new collection for 2017-18</h4>
                    <p>Asiabike marketing Team under the guidance and Leadership of our Managing Director Mr. Isthiark Farook participates...</p>
                    <a href="">Read More</a>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="news">
                    <div>
                        <img src="img/china-show.png">
                        <div>
                            <span>06</span>
                            Oct
                            2017
                        </div>
                    </div>
                    <h4>Asiabike launches new collection for 2017-18</h4>
                    <p>Asiabike marketing Team under the guidance and Leadership of our Managing Director Mr. Isthiark Farook participates...</p>
                    <a href="">Read More</a>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="news">
                    <div>
                        <img src="img/china-show.png">
                        <div>
                            <span>06</span>
                            Oct
                            2017
                        </div>
                    </div>
                    <h4>Asiabike launches new collection for 2017-18</h4>
                    <p>Asiabike marketing Team under the guidance and Leadership of our Managing Director Mr. Isthiark Farook participates...</p>
                    <a href="">Read More</a>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="news">
                    <div>
                        <img src="img/china-show.png">
                        <div>
                            <span>06</span>
                            Oct
                            2017
                        </div>
                    </div>
                    <h4>Asiabike launches new collection for 2017-18</h4>
                    <p>Asiabike marketing Team under the guidance and Leadership of our Managing Director Mr. Isthiark Farook participates...</p>
                    <a href="">Read More</a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>