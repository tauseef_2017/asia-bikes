<?php
get_header();
while(have_posts()): the_post()
?>
<section class="banner">
    <img src="<?php the_post_thumbnail_url(); ?>">
    <div class="container">
        <div class="cont">
            <h1><?php the_title(); ?></h1>
            <?php the_breadcrumb(); ?>
        </div>
    </div>
</section>
<section class="list-product">
    <div class="container">
    <?php
    $terms = get_field('categories');
    if( $terms ):
        ?>
        <div class="row">
        <?php foreach($terms as $term): ?>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="products-list">
                    <img src="<?php the_field('image', $term); ?>">
                    <h4><?php echo $term->name; ?></h4>
                    <a href="<?php echo get_term_link( $term ); ?>">Read more</a>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    <?php endif; ?>
    </div>
</section>
    <?php
endwhile;
get_footer();
?>