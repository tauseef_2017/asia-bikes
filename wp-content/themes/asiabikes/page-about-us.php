<?php
get_header();
while(have_posts()): the_post()
?>
<section class="banner">
    <img src="<?php echo get_the_post_thumbnail_url(); ?>">
    <div class="container">
        <div class="cont">
            <h1><?php the_title(); ?></h1>
            <?php the_breadcrumb(); ?>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="cont-cont">
                   <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if(have_rows('vision')): ?>
<section class="gary">
    <div class="container">
        <div class="row">
            <?php while(have_rows('vision')): the_row(); ?>
            <div class="col-xl-4 col-lg-4 col-md-4">
                <div class="vmv vision" style="background-image: url(<?php the_sub_field('image'); ?>);">
                    <div>
                        <h4><?php the_sub_field('title'); ?></h4>
                        <p><?php the_sub_field('desrciption'); ?></p>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php
    endif;

    $history = get_field('history_section');
    if( $history ):
?>
<section class="history">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="his-img">
                    <img src="<?php echo $history['image']; ?>">
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="his">
                    <h4><?php echo $history['title']; ?></h4>
                    <p><?php echo $history['description']; ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    endif;

endwhile;
get_footer();
?>