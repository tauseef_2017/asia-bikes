<?php
get_header();
while(have_posts()): the_post();
    $post_id = get_the_ID();
?>
<section class="banner">
    <img src="<?php echo get_the_post_thumbnail_url() ?>">
    <div class="container">
        <div class="cont">
            <h1>News</h1>
        </div>
    </div>
</section>
<section class="news-section">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-7">
                <div class="news-content">
                    <div>
                        <span> <?php the_date('d'); ?></span>
                        <?php echo get_the_date('M y'); ?>
                    </div>
                    <img src="<?php echo get_field('grid_image') ?>" alt="">
                </div>
                <div class="news-text">
                    <h2><?php the_title(); ?> </h2>
                    <?php the_content(); ?>
                    <?php echo sharethis_inline_buttons(); ?>
                </div>

                <div class="new-share">

                    </ul>
                </div>
            </div>


            <div class="col-xl-4 col-lg-4 col-md-5">
                <div class="Recent-news">
                    <h3 class="news-heading">Recent <span>news</span></h3>
                    <hr>
                    <?php
                    $args = array(
                        'posts_per_page' => 4,
                        'post_type'     => 'post',
                        'post__not_in' => array(
                            $post_id
                        )
                    );
                    // Custom query.
                    $query = new WP_Query( $args );
                        if ( $query->have_posts() ) :
                                while ( $query->have_posts() ) : $query->the_post()
                    ?>
                    <div class="list-news">
                        <div class="news-img">
                            <img src="<?php the_field('grid_image'); ?>" alt="">
                        </div>
                        <div class="news-short">
                            <p><?php echo mb_strimwidth(get_the_title(), 0, 40, '...'); ?></p>
                            <span><?php echo get_the_date('d M y')?></span>
                        </div>
                    </div>
                  <?php
                                endwhile;
                        endif;
                    wp_reset_query();
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>
    <?php
endwhile;
get_footer();
?>