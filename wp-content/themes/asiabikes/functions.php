<?php
/**
 * Created by PhpStorm.
 * User: Tauseef
 */

/* Config Variables */
define('ASSETS_IMAGE',get_template_directory_uri().'/assets/img/');
define('ASSETS_CSS',get_template_directory_uri().'/assets/css/');
define('ASSETS_JS',get_template_directory_uri().'/assets/js/');

/* Set up site*/
add_theme_support( 'custom-logo' );
add_theme_support( 'menu' );
add_theme_support( 'post-thumbnails' );

/* Includes */

include(get_template_directory().'/admin/ajex-routes.php');
include(get_template_directory().'/inc/menu.php');
include(get_template_directory().'/inc/enqueue.php');
include(get_template_directory().'/inc/widgets.php');
include(get_template_directory().'/inc/breadcrumb.php');

/* Action Filters*/
add_action( 'widgets_init', 'register_widgets' );
add_action('wp_enqueue_scripts','ewd_register_resources');
add_action( 'init', 'register_ewd_menus' );
add_action( 'after_setup_theme', 'wp_post_image_sizes' );
add_action('acf/init', 'my_acf_init');

/* Short Codes*/


/* Functions */

function wp_post_image_sizes() {
//    add_image_size( 'news-thumb', 556, 327, true ); // (cropped)
}

function my_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyD0fVd0YmrvGUMSz3enPPyXTAsZbgS38Gg');
}

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
    $menu = wp_get_nav_menu_object($args->menu);
    if( $args->theme_location == 'footer-social' ) {
        foreach ($items as &$item) {
            $icon = get_field('icon', $item);
            if ($icon) {
                $item->title = ' <img src="' . $icon . '" />';
            }
        }
    }
    return $items;
}


