<?php
get_header();
while(have_posts()): the_post();
?>
<section class="banner">
    <img src="<?php the_post_thumbnail_url(); ?>">
    <div class="container">
        <div class="cont">
            <h1><?php the_title(); ?></h1>
            <?php the_breadcrumb(); ?>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="awod-cont">
                   <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">

                <div class="owl-carousel owl-theme" id="owl-awd">
                   <?php
                    $posts = get_field('awards');
                         if( $posts ):
                             foreach( $posts as $post):
                                 setup_postdata($post);
                     ?>
                    <div class="item">
                        <div class="awd-div">
                            <img src="<?php the_field('award_image'); ?>">
                            <div>
								<span>
									<h5><?php the_title(); ?> </h5>
									<p><?php the_field('description'); ?></p>
								</span>
                            </div>
                        </div>
                    </div>
                    <?php
                            endforeach;
                         endif;
                   wp_reset_postdata();
                     ?>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="awd-sec">
    <div class="container-fluid">
        <div class="row gry-awd">
            <div class="col-xl-6 col-lg-6">
                <div class="row awrd-gray">
                    <div class="aw-div">
                        <?php
                        if(have_rows('left_block')):
                            while(have_rows('left_block')): the_row();
                            ?>
                        <div class="col-awd">
                            <h4><?php the_sub_field('title'); ?></h4>
                            <p><?php the_sub_field('description'); ?></p>
                        </div>
                        <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <?php
                $right = get_field('right_block');
                if($right):
                ?>
                <div class="row affiliate-color awrd" style="background-image: url(<?php echo $right['image']; ?>)">
                    <div class="affiliate awrd">
                        <h3><?php echo $right['title']; ?></h3>
                        <p><?php echo $right['description']; ?></p>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
    <?php
endwhile;
get_footer();
?>