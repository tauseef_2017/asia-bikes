<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_asia');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2W>sdI!3h|VRk|kl)*2M*jP^3i(I60x#8|rC$uZ$AcvX#|?{J(t*3bjAlaSx.S1S');
define('SECURE_AUTH_KEY',  'he?SlXpB.A(rRWkylJFOGb3:[#<AOC;=ew|s13e]$?NqzEcI*o5.2&QD~@-j_Fkz');
define('LOGGED_IN_KEY',    '`{(/?EHITC/qvi!eOS@Q-}hc+dW;YJyqTH!@7gv!eLtg}=r@WGSdCd+</6axP[D3');
define('NONCE_KEY',        'GW;R(~lulX,41xvC=2#sh:8a;p#;a_mqa(kI[{zN_6TmIG&~44qJY_EJw4=Cat!^');
define('AUTH_SALT',        '2/IVbOSwlvs0u!g)50nAqh}s2&c89tcK(-a5VAsUI-fG<S g- nrME}:>hzH!o= ');
define('SECURE_AUTH_SALT', 'Xr`tv3 sf&aw^Ehaa}<;^V0vva|umvUQBDIT9R71A]J,8(XS$Pnm{p}d&,E_4Z7P');
define('LOGGED_IN_SALT',   'Ybh]djNR?|F<*>3FO56k#azS#K%|hRY!7fC^#2^do;gV{,gey-D:y2)&ffp{K<>p');
define('NONCE_SALT',       '@]DbZojG1W&B-ok,g,q)f&8h3JP@yVn3ST:K0(V%(/}T^2;n,K<UQgCk-iJ2:/Td');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
